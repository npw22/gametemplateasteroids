#include "InputComponent.h"
#include "GameObject.h"
#include "GameFramework.h"

#define SEEN     1
#define PRESSED  2

InputComponent::InputComponent(class GameObject* Owner)
    : GameComponent(Owner, ComponentType::InputComponent)
{
    GameFramework::RegisterInputComponent(this);
}


bool InputComponent::IsKeyPressed(int KeyCode) const
{
    int result = Keys[KeyCode] & PRESSED;
    return result != 0;
}

bool InputComponent::IsKeyReleased(int KeyCode) const
{
    int result = Keys[KeyCode] & PRESSED;
    return result == 0;
}

bool InputComponent::IsKeyJustPressed(int KeyCode) const
{
    return IsKeyPressed(KeyCode) && Keys[KeyCode] & SEEN;
}

bool InputComponent::IsKeyJustReleased(int KeyCode) const
{
    return IsKeyReleased(KeyCode) && Keys[KeyCode] & SEEN;
}

bool InputComponent::IsMouseButtonPressed(int MouseButton) const
{
    int result = MouseButtons[MouseButton] & PRESSED;
    return result != 0;
}

bool InputComponent::IsMouseButtonReleased(int MouseButton) const
{
    int result = MouseButtons[MouseButton] & PRESSED;
    return result == 0;
}

bool InputComponent::IsMouseButtonJustPressed(int MouseButton) const
{
    return IsMouseButtonPressed(MouseButton) && MouseButtons[MouseButton] & SEEN;
}

bool InputComponent::IsMouseButtonJustReleased(int MouseButton) const
{
    return IsMouseButtonReleased(MouseButton) && MouseButtons[MouseButton] & SEEN;
}

void InputComponent::OnKeyPressed(int KeyCode)
{
    Keys[KeyCode] |= PRESSED;
    Keys[KeyCode] |= SEEN;
}

void InputComponent::OnKeyReleased(int KeyCode)
{
    Keys[KeyCode] &= ~PRESSED;
    Keys[KeyCode] |= SEEN;
}

void InputComponent::SetMousePos(int PosX, int PosY)
{
    MousePosX = PosX;
    MousePosY = PosY;
}

void InputComponent::OnMouseButtonPressed(int MouseButton)
{
    if (MouseButton >= 2 || MouseButton < 0)
    {
        return; //not supported
    }
    MouseButtons[MouseButton] |= PRESSED;
    MouseButtons[MouseButton] |= SEEN;
}

void InputComponent::OnMouseButtonReleased(int MouseButton)
{
    if (MouseButton >= 2 || MouseButton < 0)
    {
        return; //not supported
    }
    MouseButtons[MouseButton] &= ~PRESSED;
    MouseButtons[MouseButton] |= SEEN;
}

void InputComponent::OnUpdate(float DeltaTime)
{
    for (int i = 0; i < ALLEGRO_KEY_MAX; i++)
    {
        Keys[i] &= ~SEEN;
    } 
    for (int i = 0; i < 2; i++)
    {
        MouseButtons[i] &= ~SEEN;
    }
}