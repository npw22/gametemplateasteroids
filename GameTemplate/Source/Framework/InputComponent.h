#pragma once

#include "GameComponent.h"
#include <allegro5/keycodes.h>

using namespace std;

#define MOUSE_BUTTON_LEFT 0
#define MOUSE_BUTTON_RIGHT 1

class InputComponent : public GameComponent
{
    friend class GameFramework;
public:
    InputComponent(class GameObject* Owner);

    bool IsKeyPressed(int KeyCode) const;
    bool IsKeyReleased(int KeyCode) const;
    bool IsKeyJustPressed(int KeyCode) const;
    bool IsKeyJustReleased(int KeyCode) const;

    bool IsMouseButtonPressed(int MouseButton) const;
    bool IsMouseButtonReleased(int MouseButton) const;
    bool IsMouseButtonJustPressed(int MouseButton) const;
    bool IsMouseButtonJustReleased(int MouseButton) const;

    int GetMousePosX() const { return MousePosX; }
    int GetMousePosY() const { return MousePosY; }

protected:
    virtual void OnUpdate(float DeltaTime) override;

private:
    void OnKeyPressed(int Keycode);
    void OnKeyReleased(int Keycode);
    void SetMousePos(int PosX, int PosY);
    void OnMouseButtonPressed(int MouseButton);
    void OnMouseButtonReleased(int MouseButton);

    unsigned char Keys[ALLEGRO_KEY_MAX];
    unsigned char MouseButtons[2];
    int MousePosX = 0;
    int MousePosY = 0;
};

