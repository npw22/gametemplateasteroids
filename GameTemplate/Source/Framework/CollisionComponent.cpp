#include "CollisionComponent.h"	

CollisionComponent::CollisionComponent(class GameObject* Owner, ComponentType Type)
	: GameComponent(Owner, Type)
{
	CollisionIndex = GameFramework::RegisterCollisionComponent(this);	
}

void CollisionComponent::OnShutdown()
{
	if (CollisionIndex != -1)
	{
		GameFramework::UnregisterCollisionComponent(CollisionIndex);
	}
}

