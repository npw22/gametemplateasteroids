#pragma once

#include "GameComponent.h"
#include <string>

using namespace std;

class CollisionComponent : public GameComponent
{
	friend class GameFramework;
public:
	CollisionComponent(class GameObject* Owner, ComponentType Type);
protected:
	virtual bool DoesCollide(const CollisionComponent* OtherCollisionComponent) const = 0;
	virtual void OnShutdown() override;
private:
	int CollisionIndex = -1;
};