#pragma once
#include "CollisionComponent.h"
#include <string>

using namespace std;

class BoxCollisionComponent : public CollisionComponent
{
public:
    BoxCollisionComponent(class GameObject* Owner);

    virtual bool DoesCollide(const CollisionComponent* OtherCollisionComponent) const override;
    void SetScale(float NewScale) { Scale = NewScale; }
    void SetCollisionSize(float Width, float Height){ BoxWidth = Width; BoxHeight = Height; }
    void SetDrawDebug();
    void SetBoxOffset(float OffsetX, float OffsetY);
protected:
    virtual void OnRender() override;
    virtual void OnUpdate(float deltaTime) override;
private:
    float BoxWidth = 50.0f;
    float BoxHeight = 50.0f;
    float Scale = 1.0f;
    bool bDrawDebug = false;
    float CurrentBox[4] = { 0.0f };
    float BoxOffsetX = 0.0f;
    float BoxOffsetY = 0.0f;
    bool isOverridingCenter = false;
};

