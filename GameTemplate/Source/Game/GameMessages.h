#pragma once

enum class GameMessages : int
{
    CreateExplosion,
    GameOver,
    PlayerHurt,
    AddScore,
};