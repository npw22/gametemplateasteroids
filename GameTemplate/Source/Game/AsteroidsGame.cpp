#include "AsteroidsGame.h"

#include "Background.h"
#include "Explosion.h"
#include "Framework/BoxCollisionComponent.h"
#include "GameFlow.h"
#include "Laser.h"
#include "ParticleManager.h"
#include "Player.h"
#include "Rock.h"
#include "RockManager.h"
#include "UIText.h"

void AsteroidsGame::OnInit()
{
    GFlow = GameObject::CreateInstance<GameFlow>();
    BG = GameObject::CreateInstance<Background>();
    Player1 = GameObject::CreateInstance<Player>();
    RockMgr = GameObject::CreateInstance<RockManager>();
    ParticleMgr = GameObject::CreateInstance<ParticleManager>();
    SetWindowTitle("Fortress");
}

void AsteroidsGame::OnPostInit()
{
    if (BG)
    {
        BG->SetImage("Art/Background.png");
        BG->SetMusic("Audio/Music.wav");
        BG->SetPosition(638.0f, 360.0f);
    }

    if (Player1)
    {
        Player1->SetAvatarImage("Art/Ship.png");
        Player1->SetThrustersImage("Art/Thrusters.png");
        Player1->SetThrustersSound("Audio/Thruster.wav");
        Player1->SetPosition(638.0f, 360.0f);
    }

    if (GFlow)
    {
        GFlow->AddObjectToDisableAtStart(Player1);
        GFlow->AddObjectToDisableAtStart(RockMgr);
    }
}

void AsteroidsGame::OnUpdate(float DeltaTime)
{   
    if (GFlow->ShouldEndGame())
    {        
        SetGameOver();
        return;
    }
}
