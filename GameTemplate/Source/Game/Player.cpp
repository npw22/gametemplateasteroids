#include "Player.h"

#include "Framework/ImageComponent.h"
#include "Framework/InputComponent.h"
#include "Framework/SoundComponent.h"
#include "Framework/BoxCollisionComponent.h"
#include "Laser.h"
#include "GameObjectTypes.h"
#include "GameMessages.h"
#include "Framework/EventMessage.h"
#include <cmath>
#include <corecrt_math_defines.h>

void Player::OnInit()
{
    SetType(static_cast<int>(GameObjectTypes::Player));
    PlayerAvatarThrustersImageComponent = GameComponent::CreateInstance<ImageComponent>(this);
    PlayerAvatarThrustersImageComponent->SetVisible(false);
    PlayerAvatarImageComponent = GameComponent::CreateInstance<ImageComponent>(this);
    InputComp = GameComponent::CreateInstance<InputComponent>(this);
    ThrusterSoundComponent = GameComponent::CreateInstance<SoundComponent>(this);
    ThrusterSoundComponent->SetLooped(true);
    ThrusterSoundComponent->SetVolume(0.3f);
    Collision = GameComponent::CreateInstance<BoxCollisionComponent>(this);

    Lasers.reserve(200);
    //Collision->SetScale(0.5f);
}

void Player::OnUpdate(float DeltaTime)
{
    if (Lasers.size() > 0)
    {
        for (auto Iter = Lasers.begin(); Iter != Lasers.end();)
        {
            if ((*Iter)->IsDestroyed())
            {
                Iter = Lasers.erase(Iter);
                continue;
            }
            ++Iter;
        }
    }

    if (bExploding)
    {
         ExplodeTimer += DeltaTime;
         if (ExplodeTimer > ExplodingTime)
         {
             bExploding = false;
             bRespawning = true;
         }
         return;
    }

    if (bRespawning)
    {
        PlayerAvatarImageComponent->SetVisible(!PlayerAvatarImageComponent->IsVisible());
        RespawnTimer += DeltaTime;
        if (RespawnTimer > RespawningTime)
        {
            bRespawning = false;
            bInvulnerable = false;
            PlayerAvatarImageComponent->SetVisible(true);
        }
    }
    
    SetPosition(InputComp->GetMousePosX(), InputComp->GetMousePosY());
    constexpr auto r2d = 180.0f / M_PI;
    SetRotation(M_PI * -std::atan2(PreviousPositionY - InputComp->GetMousePosY(), PreviousPositionX - InputComp->GetMousePosX()));

    if (!bRespawning && InputComp->IsMouseButtonJustPressed(MOUSE_BUTTON_LEFT))
    {
        if (bCanMakeLaser)
        {
            CreateLaser(); 
            bCanMakeLaser = false;
        }
    }
    
    if (InputComp->IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
    {
        bCanMakeLaser = true;
    }

    PreviousPositionX = GetPositionX();
    PreviousPositionX = GetPositionY();
}

void Player::OnCollision(GameObject* Other)
{
    if (Other->GetType() == static_cast<int>(GameObjectTypes::Rock))
    {
        if (IsInvulnerable() || !IsEnabled())
        {
            return;
        }

        //create explosion at location
        {
            EventMessage Event(static_cast<int>(GameMessages::CreateExplosion));
            EventPayload PosX;
            PosX.SetAsFloat(GetPositionX());
            EventPayload PosY;
            PosY.SetAsFloat(GetPositionY());
            EventPayload Scale;
            Scale.SetAsFloat(1.0f);
            Event.payload.push_back(PosX);
            Event.payload.push_back(PosY);
            Event.payload.push_back(Scale);
            BroadcastEvent(Event);
        }
        if (HandleDeath())
        {
            EventMessage Event(static_cast<int>(GameMessages::GameOver));
            BroadcastEvent(Event);
        }
        else
        {
            EventMessage Event(static_cast<int>(GameMessages::PlayerHurt));
            EventPayload LivesLeft;
            LivesLeft.SetAsInt(GetLivesLeft());
            Event.payload.push_back(LivesLeft);
            BroadcastEvent(Event);
        }
    }
}

void Player::SetAvatarImage(string ImagePath)
{
    if (PlayerAvatarImageComponent)
    {
        PlayerAvatarImageComponent->LoadImage(ImagePath);
    }
}

void Player::SetThrustersImage(string ImagePath)
{
    if (PlayerAvatarThrustersImageComponent)
    {
        PlayerAvatarThrustersImageComponent->LoadImage(ImagePath);
    }
}

void Player::SetThrustersSound(string SoundPath)
{
    if (ThrusterSoundComponent)
    {
        ThrusterSoundComponent->LoadSample(SoundPath);
    }
}

bool Player::HandleDeath()
{
    if (ThrusterSoundComponent && ThrusterSoundComponent->IsPlaying())
    {
        PlayerAvatarThrustersImageComponent->SetVisible(false);
        ThrusterSoundComponent->Stop();
    }

    if (PlayerAvatarImageComponent)
    {
        PlayerAvatarImageComponent->SetVisible(false);
    }

    if (LivesLeft > 0)
    {
        bExploding = true;
        LivesLeft--;
        bInvulnerable = true;
        ExplodeTimer = 0.0f;
        RespawnTimer = 0.0f;
        return false;
    }
    else
    {
        SetEnabled(false);
        return true;
    }
}

void Player::CreateLaser()
{
    Laser* NewLaser = GameObject::CreateInstance<Laser>();
    float DirectionY = cos(GetRotation());
    float DirectionX = sin(GetRotation());
    NewLaser->SetPosition(GetPositionX() + (DirectionX * 30.0f),  GetPositionY() - (DirectionY * 30.0f));
    NewLaser->SetRotation(GetRotation());
    Lasers.push_back(NewLaser);
}

bool Player::IsInvulnerable() const
{
    return bInvulnerable;
}
