#pragma once

enum class GameObjectTypes : int
{
	Player = 0,
	Rock,
	Laser
};