#pragma once

#include "Framework/GameObject.h"

class ParticleManager : public GameObject
{
private:
    virtual void OnInit() override;
    virtual void OnShutdown() override;
    virtual void OnEvent(const EventMessage& Msg) override;
    void CreateExplosion(float PosX, float PosY, float Scale);
};

