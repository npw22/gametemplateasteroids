#include "Rock.h"

#include "Framework/BoxCollisionComponent.h"
#include "Framework/EventMessage.h"
#include "Framework/ImageComponent.h"
#include "GameMessages.h"
#include "GameObjectTypes.h"

void Rock::OnInit()
{
    SetType(static_cast<int>(GameObjectTypes::Rock));
    RockImage = GameComponent::CreateInstance<ImageComponent>(this);
    Collision = GameComponent::CreateInstance<BoxCollisionComponent>(this);
}

void Rock::OnPostInit()
{
    if (RockImage)
    {
        RockImage->LoadImage("Art/Rock.png");
        RockImage->SetScale((SplitsLeft + 1) * 0.33f);
    }
    if (Collision)
    {
        Collision->SetScale((SplitsLeft + 1) * 0.33f);
    }
}

void Rock::OnUpdate(float DeltaTime)
{
    SetRotation(GetRotation() + RotationSpeed * DeltaTime);
    
    SetPosition(GetPositionX() + (MovementDirectionX * MovementSpeed * DeltaTime),
        GetPositionY() + (MovementDirectionY * MovementSpeed * DeltaTime));

    LifeTimer += DeltaTime;
    if (LifeTimer > MaxLifeTime)
    {
        RequestDestroy();
    }
}

void Rock::Split()
{
    bNeedsSplit = false;

    //create explosion at location
    {
        EventMessage Event(static_cast<int>(GameMessages::CreateExplosion));
        EventPayload PosX;
        PosX.SetAsFloat(GetPositionX());
        EventPayload PosY;
        PosY.SetAsFloat(GetPositionY());
        EventPayload Scale;
        Scale.SetAsFloat((SplitsLeft + 1) * 0.33f);
        Event.payload.push_back(PosX);
        Event.payload.push_back(PosY);
        Event.payload.push_back(Scale);
        BroadcastEvent(Event);
    }

    {
        EventMessage Event(static_cast<int>(GameMessages::AddScore));
        EventPayload AdditionalScore;
        AdditionalScore.SetAsInt(5 * (GetSplitsLeft() + 1));
        Event.payload.push_back(AdditionalScore);
        BroadcastEvent(Event);
    }

    if (SplitsLeft-- == 0)
    {
        RequestDestroy();
        return;
    }

    MovementSpeed *= 1.2;
    if (RockImage)
    {
        RockImage->SetScale((SplitsLeft + 1) * 0.33f);
    }
    if (Collision)
    {
        Collision->SetScale((SplitsLeft + 1) * 0.33f);
    }
    float CurrentMovementDirectionX = MovementDirectionX;
    MovementDirectionX = MovementDirectionY;
    MovementDirectionY = CurrentMovementDirectionX;
}

void Rock::OnCollision(GameObject* Other)
{
    if (Other->GetType() == static_cast<int>(GameObjectTypes::Laser))
    {
        bNeedsSplit = true;
    }
}
