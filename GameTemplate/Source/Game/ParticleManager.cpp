#include "ParticleManager.h"
#include "GameMessages.h"
#include "Framework/EventMessage.h"
#include "Explosion.h"

void ParticleManager::OnInit()
{
    AddEventListener(static_cast<int>(GameMessages::CreateExplosion));
}

void ParticleManager::OnShutdown()
{
    RemoveEventListener(static_cast<int>(GameMessages::CreateExplosion));
}

void ParticleManager::OnEvent(const EventMessage& Msg)
{
    if (Msg.messageId == static_cast<int>(GameMessages::CreateExplosion))
    {
        if (Msg.payload.size() != 3)
        {
            return;
        }
        CreateExplosion(Msg.payload[0].GetAsFloat(), Msg.payload[1].GetAsFloat(), Msg.payload[2].GetAsFloat());
    }
}

void ParticleManager::CreateExplosion(float PosX, float PosY, float Scale)
{
    Explosion* NewExplosion = GameObject::CreateInstance<Explosion>();
    NewExplosion->SetPosition(PosX, PosY);
    NewExplosion->SetExplosionScale(Scale);
}
